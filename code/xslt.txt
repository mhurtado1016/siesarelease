<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ext="http://exslt.org/common"
                xmlns:uuid="java.util.UUID"
                xmlns:sdf="java.text.SimpleDateFormat"
                xmlns:date="java.util.Date"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:ota="http://www.opentravel.org/OTA/2003/05"
                xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
                exclude-result-prefixes="xsl uuid ext sdf date xalan s ota">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" omit-xml-declaration="yes" indent="yes" xalan:indent-amount="4" cdata-section-elements="comentario"/>

    <xsl:param name="ZeusCodigoHotel"/>

    <xsl:variable name="sdf"  select="sdf:new('yyyy-MM-dd')" />
    <xsl:variable name="day" select="(1 * 24 * 60 * 60 * 1000)"/>

    <xsl:template match="/">
        <reservas>
            <xsl:for-each select="//ota:OTA_HotelResNotifRQ/ota:HotelReservations/ota:HotelReservation">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </reservas>
    </xsl:template>

    <xsl:template match="ota:HotelReservation">
        <xsl:variable name="start">
            <xsl:call-template name="minDate">
                <xsl:with-param name="current" select="1"/>
                <xsl:with-param name="count" select="count(ota:RoomStays/ota:RoomStay)"/>
                <xsl:with-param name="minDate" select="ota:RoomStays/ota:RoomStay[1]/ota:TimeSpan/@Start"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="end">
            <xsl:call-template name="maxDate">
                <xsl:with-param name="current" select="1"/>
                <xsl:with-param name="count" select="count(ota:RoomStays/ota:RoomStay)"/>
                <xsl:with-param name="maxDate" select="ota:RoomStays/ota:RoomStay[1]/ota:TimeSpan/@End"/>
            </xsl:call-template>
        </xsl:variable>
        <reserva>
            <cabecera>
                <accion><xsl:choose>
                    <xsl:when test="@ResStatus='Book'">crear</xsl:when>
                    <xsl:when test="@ResStatus='Modify'">modificar</xsl:when>
                    <xsl:when test="@ResStatus='Cancel'">cancelar</xsl:when>
                </xsl:choose></accion>
                <interfaz>PROFITROOM</interfaz>
                <codigoHotel><xsl:value-of select="$ZeusCodigoHotel"/></codigoHotel>
                <canal><xsl:value-of select="ota:POS/ota:BookingChannel/ota:CompanyName/@Code"/></canal>
                <desde><xsl:value-of select="$start"/></desde>
                <hasta><xsl:value-of select="$end"/></hasta>
                <correlationId><xsl:value-of select="uuid:randomUUID()"/></correlationId>
                <echoToken></echoToken>
                <transactionId/>
                <codigoCanal/>
            </cabecera>
            <reservaIds>
                    <reservaId>
                        <contexto>externo</contexto>
                        <orden>1</orden>
                        <id><xsl:value-of select="ota:ResGlobalInfo/ota:HotelReservationIDs/ota:HotelReservationID[@ResID_Type='14']/@ResID_Value"/></id>
                    </reservaId>                
            </reservaIds>
            <comentarios>               
                <comentario/>
            </comentarios>
            <peticionesEspeciales>
                <xsl:for-each select="ota:ResGlobalInfo/ota:SpecialRequests/ota:SpecialRequest">
                    <peticionesEspecial>
                        <codigo><xsl:value-of select="@Name"/></codigo>
                        <descripcion><xsl:value-of select="ota:Text/ota:value"/></descripcion>
                    </peticionesEspecial>
                </xsl:for-each>
                <xsl:if test="count(ota:ResGlobalInfo/ota:SpecialRequests/ota:SpecialRequest)=0">
                    <peticionesEspecial>
                        <codigo/>
                        <descripcion/>
                    </peticionesEspecial>
                </xsl:if>
            </peticionesEspeciales>
            <servicios>
                <xsl:for-each select="ota:Services/ota:Service/ota:ServiceDetails">
                <servicio>
                    <nombre><xsl:value-of select="@Name"/></nombre>
                    <codigo><xsl:value-of select="../@ID"/></codigo>
                    <precio/>
                    <cantidad/>
                    <desde/>
                    <hasta/>
                </servicio>
                </xsl:for-each>
            </servicios>
            <datosPago>
                <medioPago>TC</medioPago>
                <tarjetahabiente></tarjetahabiente>
                <franquicia></franquicia>
                <numeroTarjeta></numeroTarjeta>
                <verificacion/>
                <vencimiento></vencimiento>
                <valorPago><xsl:value-of select="ota:ResGlobalInfo/ota:DepositPayments/ota:AmountPercent/@Amount"/></valorPago>
            </datosPago>
            <perfiles>
                <xsl:if test="count(ota:ResGlobalInfo/ota:Profiles/ota:ProfileInfo/ota:Profile/ota:Customer) > 0">
                    <perfil>
                        <tipo>huesped</tipo>
                        <categoria>adulto</categoria>
                        <id/>
                        <iata/>
                        <nombre><xsl:value-of select="ota:ResGlobalInfo/ota:Profiles/ota:ProfileInfo/ota:Profile/ota:Customer/ota:PersonName/ota:GivenName"/></nombre>
                        <apellidos><xsl:value-of select="ota:ResGlobalInfo/ota:Profiles/ota:ProfileInfo/ota:Profile/ota:Customer/ota:PersonName/ota:Surname"/></apellidos>
                        <telefono><xsl:value-of select="ota:ResGlobalInfo/ota:Profiles/ota:ProfileInfo/ota:Profile/ota:Customer/ota:Telephone/@PhoneNumber"/></telefono>
                        <email><xsl:value-of select="ota:ResGlobalInfo/ota:Profiles/ota:ProfileInfo/ota:Profile/ota:Customer/ota:Email"/></email>
                        <pais><xsl:value-of select="ota:ResGlobalInfo/ota:Profiles/ota:ProfileInfo/ota:Profile/ota:Customer/ota:Address/ota:CountryName"/></pais>
                        <codPaisCRS/>
                        <ciudad/>
                        <direccion><xsl:value-of select="ota:ResGlobalInfo/ota:Profiles/ota:ProfileInfo/ota:Profile/ota:Customer/ota:Address/ota:AddressLine"/></direccion>
                        <codigoPostal><xsl:value-of select="ota:ResGlobalInfo/ota:Profiles/ota:ProfileInfo/ota:Profile/ota:Customer/ota:Address/ota:PostalCode"/></codigoPostal>
                        <estado/>
                        <codigoEstado/>
                    </perfil>
                </xsl:if>                
            </perfiles>
            <habitaciones>
                <xsl:if test="count(ota:RoomStays/ota:RoomStay)=0">
                    <habitacion>
                        <tipo/>
                        <cantidad/>
                        <plan/>
                        <adultos/>
                        <ninos/>
                        <desde/>
                        <hasta/>
                        <moneda/>
                        <precio/>
                        <tarifas>
                            <tarifa>
                                <plan/>
                                <fecha/>
                                <valor/>
                            </tarifa>
                        </tarifas>
                    </habitacion>
                </xsl:if>
                <xsl:for-each select="ota:RoomStays/ota:RoomStay">
                    <xsl:variable name="ninos">
                        0<xsl:value-of select="ota:GuestCounts/ota:GuestCount/@ChildCount"/>
                    </xsl:variable>
                    <habitacion>
                    <xsl:variable name="cantidad_habitaciones" select="count(ota:RoomTypes/ota:RoomType)"/>
                        <tipo><xsl:value-of select="ota:RoomTypes/ota:RoomType/@RoomCode"/></tipo>
                        <cantidad><xsl:value-of select="$cantidad_habitaciones"/></cantidad>
                        <plan><xsl:value-of select="ota:RatePlans/ota:RatePlan/@RatePlanCode"/></plan>
                        <adultos><xsl:value-of select="ota:GuestCounts/ota:GuestCount/@AdultCount * $cantidad_habitaciones"/></adultos>
                        <ninos><xsl:value-of select="$ninos * $cantidad_habitaciones"/></ninos>
                        <desde><xsl:value-of select="ota:TimeSpan/@Start"/></desde>
                        <hasta><xsl:value-of select="ota:TimeSpan/@End"/></hasta>
                        <moneda><xsl:value-of select="ota:Total/@CurrencyCode"/></moneda>
                        <precio>
                            <xsl:choose>
                                <xsl:when test="ota:Total/@AmountAfterTax">
                                    <xsl:value-of select="ota:Total/@AmountAfterTax"/>
                                </xsl:when>
                                <xsl:when test="ota:Total/@AmountBeforeTax">
                                    <xsl:value-of select="ota:Total/@AmountBeforeTax"/>
                                </xsl:when>
                            </xsl:choose>
                        </precio>
                        <tarifas>
                            <xsl:for-each select="ota:RoomRates/ota:RoomRate/ota:Rates/ota:Rate[@RatePlanCode]">
                                <tarifa>
                                    <plan><xsl:value-of select="@RatePlanCode"/></plan>
                                    <fecha><xsl:value-of select="../../@EffectiveDate"/></fecha>
                                    <valor><xsl:value-of select="ota:Total/@AmountAfterTax"/></valor>
                                </tarifa>
                            </xsl:for-each>
                        </tarifas>
                    </habitacion>
                </xsl:for-each>
            </habitaciones>
        </reserva>
    </xsl:template>

    <xsl:template name="minDate">
        <xsl:param name="current"/>
        <xsl:param name="count"/>
        <xsl:param name="minDate"/>
        <xsl:choose>
            <xsl:when test="$current&lt;=$count">
                <xsl:choose>
                    <xsl:when test="date:before(sdf:parse($sdf, ota:RoomStays/ota:RoomStay[$current]/ota:TimeSpan/@Start), sdf:parse($sdf, $minDate))">
                        <xsl:call-template name="minDate">
                            <xsl:with-param name="current" select="$current+1"/>
                            <xsl:with-param name="count" select="$count"/>
                            <xsl:with-param name="minDate" select="ota:RoomStays/ota:RoomStay[$current]/ota:TimeSpan/@Start"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="minDate">
                            <xsl:with-param name="current" select="$current+1"/>
                            <xsl:with-param name="count" select="$count"/>
                            <xsl:with-param name="minDate" select="$minDate"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$minDate"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="maxDate">
        <xsl:param name="current"/>
        <xsl:param name="count"/>
        <xsl:param name="maxDate"/>
        <xsl:choose>
            <xsl:when test="$current&lt;=$count">
                <xsl:choose>
                    <xsl:when test="date:after(sdf:parse($sdf, ota:RoomStays/ota:RoomStay[$current]/ota:TimeSpan/@End), sdf:parse($sdf, $maxDate))">
                        <xsl:call-template name="maxDate">
                            <xsl:with-param name="current" select="$current+1"/>
                            <xsl:with-param name="count" select="$count"/>
                            <xsl:with-param name="maxDate" select="ota:RoomStays/ota:RoomStay[$current]/ota:TimeSpan/@End"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="maxDate">
                            <xsl:with-param name="current" select="$current+1"/>
                            <xsl:with-param name="count" select="$count"/>
                            <xsl:with-param name="maxDate" select="$maxDate"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$maxDate"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>